package com.desktop.web.core.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.role.RoleService;
import com.desktop.web.uda.entity.User;

@Aspect
@Order(1)
@Component
public class RightTargetAspect {

    private static Logger logger = LoggerFactory.getLogger(RightTargetAspect.class);

    @Autowired
    private RoleService roleService;

    @Around(value = "execution(* com.desktop.web.controller.web..*(..)) && @annotation(com.desktop.web.core.aop.RightTarget)")
    public Object check(final ProceedingJoinPoint joinPoint) throws Throwable {

        try {

            Method method = getMethod(joinPoint);
            User user = RequestUtil.getUser();
            if (user == null) {
                return Result.Error("未授权，请先登录");
            }

            if (roleService.isAdminByUid(user.getId())) {
                return joinPoint.proceed();
            }

            Annotation p = method.getAnnotation(RightTarget.class);
            Method n = RightTarget.class.getDeclaredMethod("value");
            String right = (String) n.invoke(p);
            if (StringUtils.isEmpty(right)) {
                return joinPoint.proceed();
            }

            boolean pass = roleService.hasRight(user, right);
            if (!pass) {
                return Result.Error("无权限操作，请先授权");
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        }

        return joinPoint.proceed();
    }

    private Method getMethod(JoinPoint joinPoint) {
        Method currentMethod = null;

        Signature sig = joinPoint.getSignature();
        if (sig instanceof MethodSignature) {
            try {
                MethodSignature msig = (MethodSignature) sig;
                Object target = joinPoint.getTarget();
                currentMethod = target.getClass().getMethod(msig.getName(), msig.getParameterTypes());
            } catch (Exception e) {
                logger.error("get method from JoinPoint failed, cause: {}", e.getMessage());
            }
        }

        return currentMethod;
    }

}
