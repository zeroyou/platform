/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.desktop.web.core.aop.RightTarget;
import com.desktop.web.core.comenum.RemoteProtocolType;
import com.desktop.web.core.comenum.Status;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.utils.Util;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.device.DeviceService;
import com.desktop.web.service.node.NodeService;
import com.desktop.web.uda.entity.Device;
import com.desktop.web.uda.entity.Node;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
@Controller
@RequestMapping("/webapi")
public class DeviceController extends BaseWebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private NodeService nodeService;

    @RequestMapping(value = "/device/node/devicenode/info/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "device_device_select")
    public Object getDeviceNodeInfo(HttpServletRequest request) {

        try {
            Long deviceId = Long.valueOf(request.getParameter("deviceId"));
            deviceService.checkDeviceByCuruser(deviceId);
            Node node = deviceService.getDeviceNodeInfo(deviceId);
            return Result.Success(node);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/device/node/list/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "device_device_select")
    public Object getListNodeDevice(HttpServletRequest request) {

        try {
            Long nodeId = Long.valueOf(request.getParameter("nodeId"));
            String name = request.getParameter("name");
            String ip = request.getParameter("ip");
            String devdesc = request.getParameter("devdesc");
            String isonline = request.getParameter("isonline");

            nodeService.checkNodeByCuruser(nodeId);
            List<Device> list = deviceService.getListNodeDeviceByNodeid(nodeId, name, ip, devdesc, isonline);
            return Result.Success(list);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/device/state/list/get", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_device_select")
    public Object getListDeviceState(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            List<Long> ids = Util.toArrayLong(params.get("ids").toString());
            deviceService.checkDeviceByCuruser(ids);
            return Result.Success(deviceService.getListDeviceState(ids));
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/device/delete", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_device_manager")
    public Object deleteDevice(HttpServletRequest request) {

        try {

            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            User user = this.getCurUser();
            Long deviceId = Long.valueOf(params.get("deviceId"));

            deviceService.checkDeviceByCuruser(deviceId);
            deviceService.deleteDevice(user, deviceId);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/device/update", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_device_manager")
    public Object updateDevice(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            Device update = new Device();
            update.setId(Long.valueOf(params.get("deviceId").toString()));
            update.setDevdesc(params.get("devdesc").toString());
            update.setName(params.get("name").toString());

            Device tempDevice = deviceService.getDeviceById(update.getId());
            if (tempDevice == null) {
                throw new BusinessException();
            }

            if (tempDevice.getIsprober().equals(Status.NO)) {
                update.setIp(params.get("ip").toString());
                update.setPort(Integer.valueOf(params.get("port").toString()));
                update.setProtocol(RemoteProtocolType.valueOf(params.get("protocol").toString()));
                if (update.getProtocol().equals(RemoteProtocolType.RDP) || update.getProtocol().equals(RemoteProtocolType.SSH)) {
                    update.setUsername(params.get("username").toString());
                    update.setPassword(params.get("password").toString());
                }
            }

            Long nodeId = Long.valueOf(params.get("nodeId").toString());

            deviceService.checkDeviceByCuruser(update.getId());
            User user = this.getCurUser();
            deviceService.updateDevice(user, update);
            deviceService.moveDevice(update.getId(), nodeId);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/device/add", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_device_manager")
    public Object addDevice(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                return null;
            }
            Device device = deviceService.addDevice(params);
            device.setPassword(null);
            return Result.Success(device);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/device/move", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_device_manager")
    public Object moveDevice(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            Long deviceId = Long.valueOf(params.get("deviceId").toString());
            Long nodeId = Long.valueOf(params.get("nodeId").toString());
            deviceService.checkDeviceByCuruser(deviceId);
            nodeService.checkNodeByCuruser(nodeId);

            deviceService.moveDevice(deviceId, nodeId);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/device/top/update", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_device_manager")
    public Object updateTopDevice(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            Long deviceId = Long.valueOf(params.get("deviceId").toString());
            deviceService.topDevice(deviceId);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }
}
